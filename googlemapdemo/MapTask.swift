//
//  MapTask.swift
//  googlemapdemo
//
//  Created by SGI-Mac7 on 05/11/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation
class MapTask{
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    
    var lookupAddressResults = [String:AnyObject]()
    var fetchedFormattedAddress: String!
    
    var fetchedAddressLongitude: Double!
    
    var fetchedAddressLatitude: Double!
    
    
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
    var selectedRoute = [String:AnyObject]()
    
    var overviewPolyline = [String:AnyObject]()
    
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var originAddress: String?
    
    var destinationAddress: String?
    
    
    var totalDistanceInMeters: UInt = 0
    
    var totalDistance = ""
    
    var totalDurationInSeconds: UInt = 0
    
    var totalDuration = ""
    
    var waypointStringURL = "optimize:true"
    
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        
        if  address != nil {
            
            let parameters:[String:String] = ["address":address,"key":"AIzaSyDTquQ5mqAgW2HVexRdmNXsixNYTwJPVKQ"]
            
            DispatchQueue.main.async {
                
                
                Alamofire.request(self.baseURLGeocode, method: .get, parameters: parameters).responseJSON { (response) in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if let status = json["status"].string{
                            print(status)
                        if let resultarray = json["results"].arrayObject{
                            let arrayofdict = resultarray as! [[String:AnyObject]]
                            self.lookupAddressResults = arrayofdict[0]
                            print(self.lookupAddressResults)
                            self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as? String
                            print(self.fetchedFormattedAddress)
                            
                            if let geometry =  self.lookupAddressResults["geometry"]{
                                if let location = geometry["location"] as? [String:AnyObject]{
                                    if let lat = location["lat"] as? Double{
                                        self.fetchedAddressLatitude = lat
                                    }
                                    if let lng = location["lng"] as? Double{
                                        self.fetchedAddressLongitude = lng
                                    }
                                    completionHandler(status, true)
                                    
                                }
                                                     }
                        }
                    }
                    case .failure(let error):
                        print("THE ERROR IS \(error)")
                        completionHandler("", false)
                        
                    }
                }
            }
          
            
            
        }
            
            
        else {
            completionHandler("No valid address.", false)
        }
    }
    
    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: TravelModes!, completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        if let originLocation = origin {
            if let destinationLocation = destination {
                
                if let travel = travelMode {
                    var travelModeString = ""
                    
                    switch travelMode.rawValue {
                    case TravelModes.walking.rawValue:
                        travelModeString = "walking"
                        
                    case TravelModes.bicycling.rawValue:
                        travelModeString = "bicycling"
                        
                    default:
                        travelModeString = "driving"
                    }
                    
                
                
                if let routeWaypoints = waypoints{
                    for waypoint in routeWaypoints{
                        waypointStringURL += "|" + waypoint
                    }
                    print(waypointStringURL)
                    let parameters:[String:String] = ["origin":originLocation,"destination":destinationLocation,"waypoints":waypointStringURL,"&mode":travelModeString, "key":"AIzaSyDuoFHe7NQ5U6GZ1SSu7XcckrQ9Bi8_at0"]
                    
                    
                    Alamofire.request(self.baseURLDirections, method: .get, parameters: parameters).responseJSON { (response) in
                        switch response.result {
                        case .success(let value):
                            let json = JSON(value)
                            //                           print("JSON: \(json)")
                            print("baskbdksfasfrkasdfhsadfkahsfdajksfkjafsas")
                            if json["status"].string == "OK"{
                                
                                if let routes = json["routes"].arrayObject{
                                    //                                    print(routes)
                                    
                                    self.selectedRoute  = routes[0] as! [String:AnyObject]
                                    //                                    print(self.selectedRoute)
                                    if let Polyline = self.selectedRoute["overview_polyline"]  as? [String : AnyObject]{
                                        self.overviewPolyline = Polyline
                                        print("the overview polyline is \(self.overviewPolyline)")
                                    }
                                    if let legs = self.selectedRoute["legs"] as? [[String:AnyObject]]{
                                        print(legs)
                                        if let startLocationDictionary = legs[0]["start_location"] as? [String:AnyObject]
                                        {
                                            self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                                        }
                                        if let endLocation = legs[legs.count-1]["end_location"] as? [String:AnyObject]{
                                            self.destinationCoordinate = CLLocationCoordinate2DMake(endLocation["lat"] as! Double, endLocation["lng"] as! Double)
                                            
                                        }
                                        
                                        if let startAddress = legs[0]["start_address"] as? String{
                                            self.originAddress = startAddress
                                            print(self.originAddress!)
                                            
                                        }
                                        
                                        if let destination = legs[legs.count - 1]["end_address"] as? String{
                                            self.destinationAddress = destination
                                            print(self.destinationAddress!)
                                        }
                                        
                                    }
                                    
                                }
                                self.calculateTotalDistanceAndDuration()
                            }
                            
                            
                            completionHandler("success", true)
                            
                        case .failure(let error):
                            print("THE ERROR IS \(error)")
                            completionHandler("false ", false)
                        }
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
                let parameters:[String:String] = ["origin":originLocation,"destination":destinationLocation,"&mode":travelModeString,"key":"AIzaSyDuoFHe7NQ5U6GZ1SSu7XcckrQ9Bi8_at0"]
                DispatchQueue.main.async {
                    
                    Alamofire.request(self.baseURLDirections, method: .get, parameters: parameters).responseJSON { (response) in
                        switch response.result {
                        case .success(let value):
                            let json = JSON(value)
//                           print("JSON: \(json)")
                            print("baskbdksfasfrkasdfhsadfkahsfdajksfkjafsas")
                            if json["status"].string == "OK"{
                                
                                if let routes = json["routes"].arrayObject{
//                                    print(routes)
                                    
                                   self.selectedRoute  = routes[0] as! [String:AnyObject]
//                                    print(self.selectedRoute)
                                    if let Polyline = self.selectedRoute["overview_polyline"]  as? [String : AnyObject]{
                                        self.overviewPolyline = Polyline
                                        print("the overview polyline is \(self.overviewPolyline)")
                                        }
                                    if let legs = self.selectedRoute["legs"] as? [[String:AnyObject]]{
                                        print(legs)
                                        if let startLocationDictionary = legs[0]["start_location"] as? [String:AnyObject]
                                        {
                                            self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                                        }
                                        if let endLocation = legs[legs.count-1]["end_location"] as? [String:AnyObject]{
                                            self.destinationCoordinate = CLLocationCoordinate2DMake(endLocation["lat"] as! Double, endLocation["lng"] as! Double)
                                            
                                        }
                                  
                                        if let startAddress = legs[0]["start_address"] as? String{
                                            self.originAddress = startAddress
                                            print(self.originAddress!)
                                            
                                        }
                                        
                                        if let destination = legs[legs.count - 1]["end_address"] as? String{
                                            self.destinationAddress = destination
                                            print(self.destinationAddress!)
                                        }
                                        
                                    }
                                    
                                }
                                self.calculateTotalDistanceAndDuration()
                            }
                            
                            
                        completionHandler("success", true)
                        
                        case .failure(let error):
                            print("THE ERROR IS \(error)")
                            completionHandler("false ", false)
                    }
                        }
                    }
            
            }
                }
              
            
            else {
                completionHandler("Destination is nil.", false)
            }
    }

        else {
            completionHandler("Origin is nil", false)
        }
    }
    func calculateTotalDistanceAndDuration() {
         if let legs = self.selectedRoute["legs"] as? [[String:AnyObject]]{
        totalDistanceInMeters = 0
        totalDurationInSeconds = 0
        
        for leg in legs {
            if let dist = leg["distance"] as? [String:AnyObject] {
                if let value = dist["value"] as? UInt{
                    totalDistanceInMeters += value
                    print("the total distance is \(totalDistanceInMeters)")
                }
            }
            
            if let duration = leg["duration"] as? [String:AnyObject] {
                if let value = duration["value"] as? UInt{
                    totalDurationInSeconds += value
                    print("the total duration is \(totalDurationInSeconds)")
                }
            }
            
        }
        
    }
        let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
        totalDistance = "Total Distance: \(distanceInKilometers) Km"
        
        
        let mins = totalDurationInSeconds / 60
        let hours = mins / 60
        let days = hours / 24
        let remainingHours = hours % 24
        let remainingMins = mins % 60
        let remainingSecs = totalDurationInSeconds % 60
        
        totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
    }
}
